#pragma once

#include "image.h"

struct image rotate(struct image const source, int16_t angle, bool *status);
