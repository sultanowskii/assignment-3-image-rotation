#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

// `status` must be a valid pointer.
// on failure: `*status == 0`
// on success: `*status != 0`
struct image image_create(uint64_t height, uint64_t width, bool *status);

struct image image_create_empty(void);

void image_destroy(struct image *image);

inline static uint64_t get_pixel_index_at_coords(struct image const *image, uint64_t y, uint64_t x) {
    return y * image->width + x;
}
