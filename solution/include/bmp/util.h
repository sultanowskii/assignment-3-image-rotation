#pragma once

#include <stdint.h>

#define BMP_ROW_ALIGNMENT_FACTOR 4

// Get padding size for n (= m).
// Returns value within [0; BMP_ROW_ALIGNMENT_FACTOR - 1]
//
// So, (n + ret) % BMP_ROW_ALIGNMENT_FACTOR == 0
inline static uint64_t bmp_get_padding(uint64_t n) {
    return (4 - (n % BMP_ROW_ALIGNMENT_FACTOR)) % BMP_ROW_ALIGNMENT_FACTOR;
}

// Pad n so it's divisible by BMP_ROW_ALIGNMENT_FACTOR
inline static uint64_t bmp_pad_row_size(uint64_t n) {
    return ((n + (BMP_ROW_ALIGNMENT_FACTOR - 1)) / BMP_ROW_ALIGNMENT_FACTOR) * BMP_ROW_ALIGNMENT_FACTOR;
}
