#pragma once

#include <stdint.h>

#define BMP_SIGNATURE 0x4D42

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;            // signature
    uint32_t bfileSize;         // file size
    uint32_t bfReserved;        // reserved (0)
    uint32_t bOffBits;          // pixel data offset

    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
