#pragma once

#include "bmp/io.h"

static const char* BMP_READ_STATUS_MESSAGES[__BMP_READ_STATUS_N] = {
    [BMP_READ_OK] = "BMP file is read successfully",
    [BMP_READ_INVALID_SIGNATURE] = "BMP file has invalid signature",
    [BMP_READ_INVALID_HEADER] = "BMP header is corrupted",
    [BMP_READ_IO_ERROR] = "IO error while reading BMP file",
    [BMP_READ_INVALID_SIZE_INFO] = "BMP file contains invalid size information",
    [BMP_READ_MEMORY_ALLOCATION_ERROR] = "Unexpected memory allocation error while reading BMP file"
};

static const char* BMP_WRITE_STATUS_MESSAGES[__BMP_WRITE_STATUS_N] = {
    [BMP_WRITE_OK] = "BMP file is written successfully",
    [BMP_WRITE_IO_ERROR] = "IO error while writing BMP file",
};
