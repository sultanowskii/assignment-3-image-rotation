#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "bmp/header.h"

bool bmp_is_signature_valid(struct bmp_header *header);

bool bmp_is_size_info_valid(struct bmp_header *header, FILE* f);
