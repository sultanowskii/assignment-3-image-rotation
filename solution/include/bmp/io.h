#pragma once

#include <stdio.h>

#include "image.h"

enum bmp_read_status {
    BMP_READ_OK = 0,
    BMP_READ_INVALID_SIGNATURE,
    BMP_READ_INVALID_HEADER,
    BMP_READ_IO_ERROR,
    BMP_READ_INVALID_SIZE_INFO,
    BMP_READ_MEMORY_ALLOCATION_ERROR,
    __BMP_READ_STATUS_N
};


enum bmp_write_status {
    BMP_WRITE_OK = 0,
    BMP_WRITE_IO_ERROR,
    __BMP_WRITE_STATUS_N
};

enum bmp_read_status from_bmp(FILE* in, struct image* img);
enum bmp_write_status to_bmp(FILE* out, struct image const* img);
