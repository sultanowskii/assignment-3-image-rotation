#pragma once

#include <stdint.h>

struct arguments {
    char *in;
    char *out;
    int16_t angle;
};

enum argument_parse_status {
    ARG_PARSE_OK = 0,
    ARG_PARSE_NOT_ENOUGH,
    ARG_PARSE_INVALID_ANGLE,
    __ARG_PARSE_STATUS_N
};

enum argument_parse_status parse_arguments(int argc, char* argv[], struct arguments *args);
