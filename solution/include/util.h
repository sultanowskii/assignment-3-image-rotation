#pragma once

#define SWAP_INTS(a, b) do { \
    (a) = (a) ^ (b); \
    (b) = (a) ^ (b); \
    (a) = (a) ^ (b); \
} while (0);

void eprint(const char *s);
