#pragma once

#include "args.h"

static const char* ARG_PARSE_STATUS_MESSAGES[__ARG_PARSE_STATUS_N] = {
    [ARG_PARSE_OK] = "Args parsed",
    [ARG_PARSE_NOT_ENOUGH] = "Not enough arguments provided",
    [ARG_PARSE_INVALID_ANGLE] = "`angle` has to be a valid integer",
};
