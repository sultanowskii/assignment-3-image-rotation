#include "transformation/rotate.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "util.h"

struct coords {
    uint64_t y;
    uint64_t x;
};

typedef struct coords coords_transformer(uint64_t height, uint64_t width, uint64_t y, uint64_t x);

static struct coords coords_transformer_rotate_0(
    uint64_t __attribute__ ((unused)) height,
    uint64_t __attribute__ ((unused)) width,
    uint64_t y,
    uint64_t x
) {
    return (struct coords) {
        .y = y,
        .x = x,
    };
}

static struct coords coords_transformer_rotate_90(
    uint64_t height,
    uint64_t __attribute__ ((unused)) width,
    uint64_t y,
    uint64_t x
) {
    return (struct coords) {
        .y = x,
        .x = (height - 1) - y,
    };
}

static struct coords coords_transformer_rotate_180(
    uint64_t __attribute__ ((unused)) height,
    uint64_t __attribute__ ((unused)) width,
    uint64_t y,
    uint64_t x
) {
    return (struct coords) {
        .y = (height - 1) - y,
        .x = (width - 1) - x,
    };
}

static struct coords coords_transformer_rotate_270(
    uint64_t __attribute__ ((unused)) height,
    uint64_t __attribute__ ((unused)) width,
    uint64_t y,
    uint64_t x
) {
    return (struct coords) {
        .y = (width - 1) - x,
        .x = y,
    };
}

static coords_transformer* const COORDS_TRANSFORM_ROTATORS[] = {
    coords_transformer_rotate_0,
    coords_transformer_rotate_90,
    coords_transformer_rotate_180,
    coords_transformer_rotate_270,
};

static uint8_t get_rotation_count(int16_t angle) {
    int16_t normalised_angle = angle;

    // so that the angle is within [0; 360)
    while (normalised_angle < 0) {
        normalised_angle += 360;
    }
    while (normalised_angle >= 360) {
        normalised_angle -= 360;
    }

    return normalised_angle / 90;
}

static coords_transformer* get_coords_transform_rotator(uint8_t rotation_count) {
    return COORDS_TRANSFORM_ROTATORS[rotation_count];
}

static uint8_t is_dimensions_swap_required(uint8_t rotation_count) {
    return rotation_count % 2 == 1;
}

struct image rotate(struct image const source, int16_t angle, bool *status) {
    const uint8_t rotation_count = get_rotation_count(angle);
    coords_transformer* rotate_coords = get_coords_transform_rotator(rotation_count);

    size_t new_width = source.width;
    size_t new_height = source.height;

    if (is_dimensions_swap_required(rotation_count)) {
        SWAP_INTS(new_height, new_width);
    }

    bool is_created;
    struct image new_image = image_create(new_height, new_width, &is_created);
    if (!is_created) {
        *status = false;
        return new_image;
    }

    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            struct coords new_coords = rotate_coords(source.height, source.width, y, x);

            uint64_t index = get_pixel_index_at_coords(&source, y, x);
            uint64_t new_index = get_pixel_index_at_coords(&new_image, new_coords.y, new_coords.x);

            new_image.data[new_index] = source.data[index];
        }
    }

    *status = true;
    return new_image;
}
