#include "bmp/validation.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp/header.h"
#include "bmp/util.h"
#include "image.h"
#include "io.h"

bool bmp_is_signature_valid(struct bmp_header *header) {
    return header->bfType == BMP_SIGNATURE;
}

bool bmp_is_size_info_valid(struct bmp_header *header, FILE* f) {
    const size_t pixel_data_offset = header->bOffBits;

    const size_t row_byte_size = header->biWidth * sizeof(struct pixel);
    const size_t expected_pixel_data_size = bmp_pad_row_size(row_byte_size) * header->biHeight;

    const size_t file_size = get_file_size(f);
    const size_t actual_pixel_data_size = file_size - pixel_data_offset;

    return actual_pixel_data_size >= expected_pixel_data_size;
}
