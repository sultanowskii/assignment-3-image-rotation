#include "args.h"

#include <stdio.h>
#include <stdlib.h>

enum argument_parse_status parse_arguments(int argc, char* argv[], struct arguments *args) {
    if (argc < 4) {
        return ARG_PARSE_NOT_ENOUGH;
    }

    args->in = argv[1];
    args->out = argv[2];

    char *end = NULL;
    int16_t parsed_angle = (int16_t)strtol(argv[3], &end, 10);
    if (end == NULL || *end != '\0') {
        return ARG_PARSE_INVALID_ANGLE;
    }
    args->angle = parsed_angle;

    return ARG_PARSE_OK;
}
