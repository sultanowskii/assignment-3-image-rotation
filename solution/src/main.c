#include <stdio.h>
#include <stdlib.h>

#include "args.h"
#include "bmp/io.h"
#include "bmp/msg.h"
#include "msg.h"
#include "transformation/rotate.h"
#include "util.h"

int main(int argc, char** argv) {
    struct arguments args = { 0 };

    enum argument_parse_status arg_parse_status = parse_arguments(argc, argv, &args);
    if (arg_parse_status != ARG_PARSE_OK) {
        eprint(ARG_PARSE_STATUS_MESSAGES[arg_parse_status]);
        return 1;
    }

    FILE *in = fopen(args.in, "r");
    if (in == NULL) {
        perror("Couldn't open IN file");
        return 1;
    }

    struct image image = {0};

    enum bmp_read_status read_status = from_bmp(in, &image);
    fclose(in);
    if (read_status != BMP_READ_OK) {
        eprint(BMP_READ_STATUS_MESSAGES[read_status]);
        return 1;
    }

    FILE *out = fopen(args.out, "w");
    if (out == NULL) {
        perror("Couldn't open OUT file");
        return 1;
    }

    bool is_rotated;
    struct image rotated_image = rotate(image, args.angle, &is_rotated);
    if (!is_rotated) {
        eprint("Rotation failed");
    }

    image_destroy(&image);

    enum bmp_write_status write_status = to_bmp(out, &rotated_image);
    fclose(out);
    image_destroy(&rotated_image);
    if (write_status != BMP_WRITE_OK) {
        eprint(BMP_WRITE_STATUS_MESSAGES[write_status]);
        return 1;
    }

    return 0;
}
