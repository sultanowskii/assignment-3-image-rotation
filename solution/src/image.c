#include "image.h"

#include <stdbool.h>
#include <stdlib.h>

struct image image_create(uint64_t height, uint64_t width, bool *status) {
    struct pixel *pixel_data = malloc(width * height * sizeof(struct pixel));

    // if its NULL - failure (0), if its not - success (1)
    *status = (pixel_data != NULL);

    return (struct image) {
        .height = height,
        .width = width,
        .data = pixel_data
    };
}

void image_destroy(struct image *image) {
    free(image->data);
    image->height = 0;
    image->width = 0;
    image->data = NULL;
}
