#include "util.h"

#include <stdio.h>

void eprint(const char *s) {
    fprintf(stderr, "%s", s);
}
